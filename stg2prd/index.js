var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
// stgのホストURL
var url_stg = 'mongodb://aidemy-db-stg.documents.azure.com:10255/aidemy?ssl=true';
// prdのホストURL
var url_prd = 'mongodb://db-stg2prd-test.documents.azure.com:10255/aidemy?ssl=true';

module.exports = function (context, req) {
    context.log('!!!Start!!!');
    context.log(req.body);
    if ((req.query.pass === 'stg2prd')) {
        context.log('Start to connect mongo');
        MongoClient.connect(
            url_stg,
            {
                auth: {
                    user: 'aidemy-db-stg',
                    password: 'rShBntGBwjkxDAs9pL7VFjh5qc9i5r2AXdMG6DFGYwboZXNjdWPgHpzj4P56OS1OqPL8Jdrmh8e5qRyR9HpGDQ=='
                }
            },
            (err, client_stg) => {
                if (err) {
                    context.log('Failed to connect stgDB');
                    context.log(err);
                    context.res = {
                        status: 500,
                        body: err
                    };
                    context.done();
                    return;
                }
                context.log('Success to connect stgDB');
                var db_stg = client_stg.db('aidemy');

                MongoClient.connect(
                    url_prd,
                    {
                        auth: {
                            user: 'db-stg2prd-test',
                            password: 'yJ8acdQAlE93NLhCpP4x3h6R8szmuk25bQPrE7LehO9xf86fT2UrCI45j3CxQjvxJJRRfnpJJszB1RBk1JX7vg=='
                        }
                    },
                    (err, client_prd) => {
                        if (err) {
                            context.log('Failed to connect prdDB');
                            context.log(err);
                            context.res = {
                                status: 500,
                                body: err
                            };
                            context.done();
                            return;
                        }
                        context.log('Success to connect prdDB');
                        var db_prd = client_prd.db('aidemy');
                        console.log('db_stg', db_stg);
                        var cursor = db_prd.collection('courses').find();
                        cursor.each((err,doc)=>{
                            console.log('doc',doc);
                        })
                        findAllCoursesFromStaging(db_stg, function (contentsCursor) {
                            // console.log('cursor',contentsCursor);
                            contentsCursor.each((err, doc) => {
                                console.log('doc', doc);
                            });
                            console.log('found');
                            findDocuments(db_stg, function () {
                                context.res = {
                                    // status: 200, /* Defaults to 200 */
                                    body: "updated"
                                };
                                context.log('updated')
                                client.close();
                                context.done();
                            });
                        });
                    });
            });
    }
    else {
        context.res = {
            status: 400,
            body: "Please pass a name on the query string or in the request body"
        };
        context.done();
    }
};

var findAllCoursesFromStaging = function (db, callback) {
    console.log('findする!!')
    var contentsCursor = db.collection('courses').find();
    console.log('cursor');
    let contentsList = [];
    contentsCursor.forEach(function (doc) {
        contentsList.push(doc);
        console.log('push');
        console.log(doc);
    })
    console.log('contentsCursor',contentsCursor);
    console.log('contentsList',contentsList);
    callback(contentsCursor);
    // return cursor;
};

// var insertDocument = function (context, db, params, callback) {
//     context.log('start insert');
//     db.collection('users').insertOne({
//         "_id": ObjectId(),
//         "email": params.email,
//         "country": params.country,
//         "attribute": params.attribute
//     }, function (err, result) {
//         // assert.equal(err, null);
//         console.log(result);
//         console.log("Inserted a document into the users collection.");
//         callback();
//     });
// };

var findDocuments = function (db, callback) {

    var cursor = db.collection('courses').findOne();
    cursor.each(function (err, doc) {
        // assert.equal(err, null);
        if (doc != null) {
            console.dir(doc);
            console.log(doc);
            console.log('found');
        } else {
            console.log('doc', doc);
            callback();
        }
    });
};